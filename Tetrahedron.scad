chamber_depth=88;// mm
thickness=0;
golden_ratio=1.618;

module pyramid() {
height=chamber_depth;
radius=(height/golden_ratio)/2;
polyhedron(
  points=[ 
  [radius,radius,0],
  [radius,-radius,0],
  [-radius,-radius,0],
  [-radius,radius,0], // the four points at base
   [0,0,height]  ],                                 // the apex point 
  faces=[ [0,1,4],
  [1,2,4],[2,3,4],[3,0,4],              // each triangle side
              [1,0,3,2] 
              ]                         // two triangles for square base
 );
}
module dowel() {
// dowel place
dowel_thickness = 25.4 *3/4;
dowel_radius = dowel_thickness/2;
dowel_height = dowel_thickness * golden_ratio;
cylinder(h = dowel_height, r = dowel_radius);
}

module dowel_hold() {
translate([-chamber_depth/golden_ratio/2,0,-5]){
    rotate([0,32,0]) {dowel();}}
}

module pyramid2() {
    height = chamber_depth;
    radius = (height/golden_ratio);
     cylinder(height,radius,00,$fn=4);
}
module tetrahedron() {
    height = chamber_depth;
    radius = (height);
     cylinder(height,radius,00,$fn=3);
}

//difference(){
    tetrahedron();
    /*dowel_hold();
    mirror([1,0,0]){dowel_hold();}
    mirror([1,1,0]){dowel_hold();}
    mirror([1,-1,0]){dowel_hold();}   */
   
//}


//linear_extrude(height = 15, length = 10, center = true, convexity = 3)
// import (file = "OSIL.dxf", layer = "SCAD.origin", origin = [0, 0]); 